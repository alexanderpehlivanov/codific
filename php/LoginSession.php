<?php
/*
* Login Session Class
*/
class loginSession
{
	private $key = '123456loginSession';
	private $userId;
	function __construct()
	{
		$conn=mysqli_connect("localhost","root","","codific");
	    // Check connection
		if (mysqli_connect_errno()) {
		  echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
		$this->db = $conn;
		session_start();
	}
	public function startSession ( $user ) {
		$sql = " SELECT * FROM user WHERE firstName = '$user' ";
		$result = $this->db->query($sql);
		if ( $result->num_rows > 0 ) {
			$row = $result->fetch_assoc();
			$_SESSION['User_Id'] = $row['id'];
			$_SESSION['User'] = $row['firstName'];
			$_SESSION['Profile_Picture'] = $row['profilePicture'];
		}
	}
	public function getId ( $getAccess ) {
		if ( $this->key == $getAccess ) {
			$this->userId = $_SESSION['User_Id'];
			return $this->userId;
		}
	}
	public function getProfilePicture ( $getAccess ) {
		if ( $this->key == $getAccess ) {
			$imgDir = 'profilePicture/'.$_SESSION['Profile_Picture'];
			return $imgDir;
		}
	}
	function __destruct() {
		if ( !isset($_SESSION['User']) ) {
			# close connection
			mysqli_close($this->db);
			if ( isset($_SESSION['Workman']) ) {
			header('Location: http://127.0.0.1/Codific/WorkmanProfil.php');
		    } else header('Location: http://127.0.0.1/Codific/index.html');
		}
	}
}
?>