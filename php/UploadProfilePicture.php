<?php
/* # Adding Class File # */
 require_once('ClassUser.php');
/* # File For Escape Function # */
 require_once('CleanInput.php');
 $newUpload = $newUser = new User();
 $upload_dir = '../profilePicture/';
 $length = 10;
 $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
 $upload_type = '.'.pathinfo($_FILES["upload"]["name"], PATHINFO_EXTENSION);
 $allowed_ext = array('jpeg','png','gif');

$file = $_FILES['upload'];
function isImage()
{
    global $file;
    $fh = fopen($_FILES["upload"]["tmp_name"],'rb');
    if ($fh) { 
        $bytes = fread($fh, 6); // read 6 bytes
        fclose($fh);            // close file

        if ($bytes === false) { // bytes there?
            return false;
        }

        // ok, bytes there, lets compare....

        if (substr($bytes,0,3) == "\xff\xd8\xff") { 
            return 'image/jpeg';
        }
        if ($bytes == "\x89PNG\x0d\x0a") {
            return 'image/png';       
        }
        if ($bytes == "GIF87a" or $bytes == "GIF89a") { 
            return 'image/gif';       
        }

        return 'application/octet-stream';
    }
    return false;
}

if (isImage() == 'image/jpeg' || isImage() == 'image/png' || isImage() == 'image/gif') {

if(array_key_exists('upload',$_FILES) && $_FILES['upload']['error'] == 0 ){
    $pic = $_FILES['upload'];
    $upload_name_tabble = $randomString.$upload_type;
    $upload_dir_name = $upload_dir.$upload_name_tabble;

    if(move_uploaded_file($pic['tmp_name'], $upload_dir_name)){
    	$newUpload->uploadProfilePicture( $upload_name_tabble, '123456Upload' );
    }

} 

}
header("location: http://127.0.0.1/Codific/Profil.php");
?>