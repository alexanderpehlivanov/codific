<?php
/* * Class User
*
*/
require_once('../includes/db.php');
class User
{
	/* # Variables #
	   - First Name
	   - Family Name
	   - Age
	   - Profile Picture
	   - Sex
	   - Password
	   - Security Question
	   - Answer
	   - Second Email
	   - Cars
	   - Access Password
	   # Check User Variables
	*/
	private $firstName;
    private $familyName;
    private $age;
    private $profilePicture;
    private $sex;
    private $password;
    private $securityQuestion;
    private $answer;
    private $secondEmail;
    private $cars;
	private $passwordAccess = '123456CreateNewUser';
	private $boolUserCheck;
	private $boolEmailCheck;
	private $passwordAccessCheck = '123456CheckPostedUser';
	#Login
	private $passwordAccessLogin = '123456Login';
	private $uploadAccessKey = '123456Upload';
	#Default Constructor Function
	function __construct()
	{
		$this->firstName = 'unknow';
		$this->familyName = "unknow";
		$this->age = "unknow";
		$this->sex = 'unknow';
		$this->password = 'unknow';
		$this->securityQuestion = "unknow";
		$this->answer = "unknow";
		$this->secondEmail = 'unknow';
		# Database
		global $conn;
		$this->db = $conn;
	}
	/* # About Functions #
	  * Create User
	  # function: newUser 
	    - First Name
	    - Family Name
	    - Age
	    - Sex
	    - Password
	    - Security Question
	    - Answer
	    - Second Email
	  # Check User
	    - Check Username
	    - Check Email
	  # function: createUser
	    - create sql string
	    - create query request
	  * Login Function
	  * Update Cars Information - There is no sense because when basic user uploads his cars every car gets id of the user
	    - Cars
	  * Update Profil Picture
	    - Profile Picture
	*/
	public function checkUser ( $getUser, $getEmail, $checkAccess ) {
		if ( $this->passwordAccessCheck == $checkAccess ) {
			$sqlUserCheck = " SELECT * FROM user WHERE firstName = '$getUser' ";
			$sqlEmailCheck = " SELECT * FROM user WHERE secondEmail = '$getEmail' ";
			$resultUserCheck = $this->db->query($sqlUserCheck);
			$resultEmailCheck = $this->db->query($sqlEmailCheck);
			if ( $resultUserCheck->num_rows > 0 ) {
				$this->boolUserCheck = 1;
			} else $this->boolUserCheck = 0;
			if ( $resultEmailCheck->num_rows > 0 ) {
				$this->boolEmailCheck = 1;
			} else $this->boolEmailCheck = 0;
		}
	}
	public function newUser( $getFirstName, $getFamilyName, $getAge, $getSex, $getPassword, $getSecurityQuestion, $getAnswer, $getSecondEmail ) {
		$this->firstName = $getFirstName;
		$this->familyName = $getFamilyName;
		$this->age = $getAge;
		$this->sex = $getSex;
		$this->password = $getPassword;
		$this->securityQuestion = $getSecurityQuestion;
		$this->answer = $getAnswer;
		$this->secondEmail = $getSecondEmail;
	}
	public function createUser($Access) {
		if ( $this->passwordAccess == $Access ) {
			if ( $this->boolUserCheck == 0 ) {
			if ( $this->boolEmailCheck == 0 ) {
			if ( !empty( $this->firstName ) && $this->firstName != 'unknow' )
				if ( !empty( $this->familyName) && $this->familyName!= 'unknow' )
					if ( !empty( $this->age ) && $this->age != 'unknow' )
						if ( !empty( $this->sex ) && $this->sex != 'unknow' ) {
							if ( !empty( $this->password ) && $this->password != 'unknow' ) {
								if ( !empty( $this->securityQuestion ) && $this->securityQuestion != 'unknow' ) {
									if ( !empty( $this->answer ) && $this->answer != 'unknow' ) {
										if ( !empty( $this->secondEmail ) && $this->secondEmail != 'unknow' ) {
							                $sql = "INSERT INTO user (firstName, familyName, age, sex, password, securityQuestion, answer, secondEmail) VALUES ('$this->firstName', '$this->familyName', '$this->age', '$this->sex', '$this->password', '$this->securityQuestion', '$this->answer', '$this->secondEmail')";
							                $query = $this->db->query($sql);
										}
									}
								}
							}
							
						}
					} # End of Email Check
				} # End of User Check
		}
	} # End createUser
	public function login ( $username, $password, $loginAccess ) {
		if ( $this->passwordAccessLogin == $loginAccess ) {
			if ( !empty($username) && !empty($password) ) {
				require_once('LoginSession.php');
				$ses = new loginSession();
				$sql = " SELECT * FROM user WHERE firstName = '$username' AND password = '$password' ";
				$result = $this->db->query($sql);
				if ( $result->num_rows > 0 ) {
					$ses->startSession($username);
					header("location: ../Profil.php"); 
				} else header("location: ../index.html");
			}
		}
	}
	# Upload Profile Picture
	public function uploadProfilePicture ( $getProfilePictureName, $getKey ) {
		if( $this->uploadAccessKey == $getKey) {
			require_once('LoginSession.php');
		    $ses = new loginSession();
		    $getUserId = $ses->getId( '123456loginSession' );
			$sql = " UPDATE user SET profilePicture = '$getProfilePictureName' WHERE id = $getUserId ";
			$result = $this->db->query($sql);
		}
	}
}
?>