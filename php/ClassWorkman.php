<?php
/* 
* Class Workman
*/
require_once('../includes/db.php');

class Workman {
	/* # Variables #
	   - First Name
	   - Last Name
	   - Password
	   - Email
	   - Profession
	   - Payment Per Hour
	   - Access Key
	   # Check User Variables
	*/
	private $firstName;
	private $lastName;
	private $password;
	private $email;
	private $profession;
	private $paymentPerHour;
	private $key = '123456CreateWorkman';
	private $boolUserCheck;
	private $boolEmailCheck;
	private $bin;
	function __construct()
	{
		$this->firstName = 'unknow';
		$this->lastName = "unknow";
		$this->password = 'unknow';
		$this->email = 'unknow';
		$this->profession = 'unknow';
		$this->paymentPerHour = 'unknow';
		# Database
		global $conn;
		$this->db = $conn;
	}
	public function checkWorkman ( $getUser, $getEmail, $checkAccess ) {
		if ( $this->key == $checkAccess ) {
			$sqlUserCheck = " SELECT * FROM workman WHERE firstName = '$getUser' ";
			$sqlEmailCheck = " SELECT * FROM workman WHERE email = '$getEmail' ";
			$resultUserCheck = $this->db->query($sqlUserCheck);
			$resultEmailCheck = $this->db->query($sqlEmailCheck);
			if ( $resultUserCheck->num_rows > 0 ) {
				$this->boolUserCheck = 1;
			} else $this->boolUserCheck = 0;
			if ( $resultEmailCheck->num_rows > 0 ) {
				$this->boolEmailCheck = 1;
			} else $this->boolEmailCheck = 0;
		}
	}
	public function newWorkman( $getFirstName, $getLastName, $getPassword, $getEmail, $getProfession, $getPaymentPerHour ) {
		$this->firstName = $getFirstName;
		$this->lastName = $getLastName;
		$this->password = $getPassword;
		$this->email = $getEmail;
		$this->profession = $getProfession;
		$this->paymentPerHour = $getPaymentPerHour;
	}
	public function createWorkman($Access) {
		if ( $this->key == $Access ) {
			if ( $this->boolUserCheck == 0 ) {
			if ( $this->boolEmailCheck == 0 ) {
			if ( !empty( $this->firstName ) && $this->firstName != 'unknow' )
				if ( !empty( $this->lastName) && $this->lastName!= 'unknow' )
					if ( !empty( $this->password ) && $this->password != 'unknow' ) {
						if ( !empty( $this->email ) && $this->email != 'unknow' ) {
							if ( !empty( $this->profession ) && $this->profession != 'unknow' ) {
								if ( !empty( $this->paymentPerHour ) && $this->paymentPerHour != 'unknow' ) {
									$sql = "INSERT INTO workman (firstName, lastName, password, email, profession, paymentPerHour) VALUES ('$this->firstName', '$this->lastName', '$this->password', '$this->email', '$this->profession', '$this->paymentPerHour')";
							        $query = $this->db->query($sql);
								}
							}
						}
					}	
				} # End of Email Check
			} # End of User Check
		}
	} # End createWorkman
	public function login ( $username, $password, $loginAccess ) {
		if ( $this->key == $loginAccess ) {
			if ( !empty($username) && !empty($password) ) {
				require_once('WorkmanLoginSession.php');
				$ses = new WorkmanLoginSession();
				$sql = " SELECT * FROM workman WHERE firstName = '$username' AND password = '$password' ";
				$result = $this->db->query($sql);
				if ( $result->num_rows > 0 ) {
					$ses->startSession($username);
					header("location: ../WorkmanProfil.php"); 
				} else header("location: ../index.html");
			}
		}
	}
	public function searchBySalary ( $getSalary, $Access ) {
		if ( $this->key == $Access ) {
			if ( !empty($getSalary) ) {
				$sql = " SELECT * FROM workman WHERE paymentPerHour LIKE '%$getSalary%' ";
				$result = $this->db->query($sql);
				if ( $result->num_rows > 0 ) {
					$this->bin = '<div>Workmans with salary '.$getSalary.'</div>';
					while( $row = $result->fetch_assoc() ) {
						$this->bin = $this->bin.'<div><a href="http://127.0.0.1/Codific/WorkmanList.php?id='.$row['id'].'">'.$row['firstName'].' '.$row['lastName'].'</a></div>';
					}
				} else $this->bin = '<div>none</div>';
				return $this->bin;
			}
		}
	}
	public function searchByProfession ( $getProfession, $Access ) {
		if ( $this->key == $Access ) {
			if ( !empty($getProfession) ) {
				$sql = " SELECT * FROM workman WHERE profession LIKE '%$getProfession%' ";
				$result = $this->db->query($sql);
				if ( $result->num_rows > 0 ) {
					$this->bin = '<div>Workmans with profession '.$getProfession.'</div>';
					while( $row = $result->fetch_assoc() ) {
						$this->bin = $this->bin.'<div><a href="http://127.0.0.1/Codific/WorkmanList.php?id='.$row['id'].'">'.$row['firstName'].' '.$row['lastName'].'</a></div>';
					}
				} else $this->bin = '<div>none</div>';
				return $this->bin;
			}
		}
	}
	public function commentWorkman ( $getId, $getComment, $Access ) {
		if ( $this->key == $Access ) {
			if ( !empty($getComment) && !empty($getId) ) {
				$sql = "INSERT INTO comments (workman_id, comment) VALUES ($getId, '$getComment')";
				$query = $this->db->query($sql);
			}
		}
	}
}
?>