<?php
require_once('includes/db.php');
function cleanInput($input) {
 
  $search = array(
    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
  );
 
    $output = preg_replace($search, '', $input);
    return $output;
  }
function sanitize($input) {
  global $conn;
    if (is_array($input)) {
        foreach($input as $var=>$val) {
            $output[$var] = sanitize($val);
        }
    }
    else {
        if (get_magic_quotes_gpc()) {
            $input = stripslashes($input);
        }
        $input  = cleanInput($input);
        $output = mysqli_real_escape_string($conn, $input);
    }
    return $output;
}
$getGetId = sanitize($_GET['id']);
$sql = " SELECT * FROM workman WHERE id = $getGetId ";
$result = $conn->query($sql);
if ( $result->num_rows > 0 ) {
$row = $result->fetch_assoc();
$firstName = $row['firstName'];
$id = $row['id'];
}
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $firstName; ?></title>
</head>
<body>
<div>Comment</div>
<form  method="post" action=<?php echo '"php/CommentWorkman.php?id='.$id.'"';?> enctype="multipart/form-data">
      <textarea rows="4" cols="50" name="comment" placeholder="Comment" required></textarea>
      <input type="submit" name="submit">
</form>
<div>Comments</div>
<?php
$sql = " SELECT * FROM comments WHERE workman_id = $id ";
				$result = $conn->query($sql);
				if ( $result->num_rows > 0 ) {
					while( $rowComment = $result->fetch_assoc() ) {
						echo '<div>'.$rowComment['comment'].'</div>';
					}
				}
?>
</body>
</html>